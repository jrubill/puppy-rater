FROM node:lts-alpine as builder

WORKDIR /frontend

ENV VUE_APP_BASE_URL=/puppies

COPY front-legs/ ./

RUN npm install

RUN npm run build

FROM python:3.8

WORKDIR /app

COPY back-legs/ ./

COPY --from=builder /frontend/dist ./static/

RUN pip3 install -r requirements.txt

EXPOSE 80

CMD gunicorn app:app -b 0.0.0.0:$PORT -w 2 -k uvicorn.workers.UvicornWorker
