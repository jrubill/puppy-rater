
let BASE_URL = process.env.VUE_APP_BASE_URL;
let PAGE_SIZE = 20;

async function getPuppers() {
    console.log(BASE_URL);
    let response = await fetch(BASE_URL);
    let data = await response.json();
    return data;
}

async function fetchPupperPage(offset) {
    let response = await fetch(`${BASE_URL}?offset=${offset}&page_size=${PAGE_SIZE}`);
    let data = await response.json();
    return data;
}

async function ratePupper(pupperID, rating) {
    await fetch(`${BASE_URL}/${pupperID}/rate/${rating}`, {
        method: 'PUT'
    });
}

export { getPuppers, ratePupper, fetchPupperPage };

