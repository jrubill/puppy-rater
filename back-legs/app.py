from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import FileResponse
from fastapi.staticfiles import StaticFiles
from airtable import Airtable 
from starlette.config import Config
from pydantic import BaseModel
import aiohttp
import os
class BasePuppy(BaseModel):
    url: str
    name: str

class PutPuppy(BasePuppy):
    rating: int
    num_ratings: int

class ReturnPuppy(PutPuppy):
    id: str

class CreatePuppy(BasePuppy):
    pass

from collections import OrderedDict

class LRUCache:
    def __init__(self, size=100):
        self.cache = OrderedDict()
        self.size = size
    
    def get(self, key):
        if key not in self.cache:
            return -1
        self.cache.move_to_end(key)
        return self.cache[key]
    
    def put(self, key, value):
        self.cache[key] = value 
        self.cache.move_to_end(key) 
        if len(self.cache) > self.size: 
            self.cache.popitem(last = False) 

cache = LRUCache()


config = Config('.env')

AIRTABLE_BASE = 'https://api.airtable.com/v0'
AIRTABLE_URL   = config('BACKLEG_STORAGE', cast=str)
AIRTABLE_TABLE = config('BACKLEG_STORAGE_TABLE', cast=str)
AIRTABLE_KEY   = config('BACKLEG_STORAGE_KEY', cast=str)

app = FastAPI()

origins = ["http://localhost:8080"]

app.mount('/dist', StaticFiles(directory="static/dist/"))

if config('ENV') == "DEV":
    app.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["OPTION, GET, PUT, POST"],
        allow_headers=["*"],
    )

airtable = Airtable(api_key=AIRTABLE_KEY, base_key=AIRTABLE_URL, table_name=AIRTABLE_TABLE)


@app.get("/")
async def index():
    return FileResponse('static/index.html')

@app.get("/puppies")
def get_puppies():
    return [x["fields"] for x in airtable.get_all()]

@app.get("/puppies/{puppy_id}")
def get_puppy(puppy_id: int):
    pup = cache.get(puppy_id)
    if pup == -1:
        pup = airtable.search('ID', puppy_id)
        if len(pup) <= 0:
            raise HTTPException(status_code=404, detail="pupper not found")
        pup = pup[0]['fields']
        cache.put(puppy_id, pup)
    
    return pup


async def fetch_puppies(offset: int = 0, page_size: int = 20):
    headers = {"Authorization": f"Bearer {AIRTABLE_KEY}"}
    params = {
        "offset": offset,
        "pageSize": page_size
    }
    async with aiohttp.ClientSession(headers=headers) as session:
        async with session.get(f"{AIRTABLE_BASE}/{AIRTABLE_URL}/{AIRTABLE_TABLE}", params=params) as r:
            return await r.json()

'''
@app.get("/puppies")
async def get_puppies_offset(offset: int = 0, page_size: int = 20):
    data = await fetch_puppies(offset, page_size)
    try:
        return [x['fields'] for x in data['records']]
    except KeyError:
        raise HTTPException(status_code=422, detail="invalid format")
'''
@app.post("/puppies")
def create_puppy(puppy: CreatePuppy):
    pup = puppy.dict()
    pup['rating'] = 0
    pup['num_ratings'] = 0
    airtable.insert(pup)

@app.put("/puppies/{puppy_id}")
def put_puppy(puppy_id: int, puppy: PutPuppy):
    airtable.update_by_field('ID', puppy_id, puppy.dict())


def compute_avg(avg: float, count: int, new_val: int):
    avg -= avg / count
    avg += new_val / float(max(count,1))
    return avg

# def not thread safe
# needs optimization
@app.put("/puppies/{puppy_id}/rate/{rating}")
def upvote_puppy(puppy_id: int, rating: float):
    pup = cache.get(puppy_id)
    if pup == -1:
        pup = airtable.search('ID', puppy_id)
        if len(pup) <= 0:
            raise HTTPException(status_code=404, detail="pupper not found")
        pup = pup[0]['fields']
        cache.put(pup['ID'], pup)
    
    if 'ID' in pup:
        del pup['ID']

    pup["num_ratings"] += 1
    pup["rating"] = compute_avg(pup["rating"], pup["num_ratings"], rating)
    airtable.update_by_field('ID', puppy_id, pup)
